# Back-end build container
版本資訊：\
node version = 18.16.0 \
yarn version = 3.2.3

## 建立docker-bridge-network
```bash
docker network create \
 --driver bridge backstage-bridge
```

可參考https://godleon.github.io/blog/Docker/docker-network-bridge/

## install postgres image
```bash
docker pull postgres:14.5
```

```bash
docker run --name postgres \
 --network backstage-bridge \
 -e POSTGRES_PASSWORD=backsta9e \
 -p 5432:5432 \
 -d postgres:14.5
```

## Follow the step 
```bash
yarn install
```
```bash
yarn tsc
```
```bash
yarn build:backend --config app-config.yaml
```
```bash
docker image build . \
 -f packages/backend/Dockerfile \
 --tag backstage-backend:1.0.0
```
複製app-config.yaml到本機電腦，修改Front-end_HOST、Back-end_HOST、DB連線資訊

可使用 
```bash
docker network inspect backstage-bridge
```
來查詢

```bash
docker run -d \
 --network backstage-bridge \
 -p 7007:7007  \
 -v $PWD/app-config.yaml:/app/app-config.yaml  \
 -e TZ=Asia/Taipei  \
 --name backstage-backend backstage-backend:1.0.0
```


