/*
 * Copyright 2020 The Backstage Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {
  RadarRing,
  RadarQuadrant,
  RadarEntry,
  TechRadarLoaderResponse,
  TechRadarApi,
} from './api';

import radarData from './radar.json'; // This import style requires "esModuleInterop", see "side notes"


const rings = new Array<RadarRing>();
for (const field of radarData.rings) {
  rings.push({ id: field.id, name: field.name, color: field.color });
}


const quadrants = new Array<RadarQuadrant>();
for (const quadrantField of radarData.quadrants) {
  quadrants.push({ id: quadrantField.id, name: quadrantField.name });
}


const entries = new Array<RadarEntry>();
for (const field of radarData.entries) {
  entries.push({
    timeline: [
      {
        moved: field.timeline[0].moved,
        ringId: field.timeline[0].ringId,
        date: new Date(field.timeline[0].date),
        description: field.timeline[0].description,
      },
    ],
    key: field.key,
    id: field.id,
    title: field.title,
    quadrant: field.quadrant,
    links: [
      {
        url: field.links[0].url,
        title: field.links[0].title,
      },
    ],
    description: field.description,
  });
}


export const mock: TechRadarLoaderResponse = {
  entries,
  quadrants,
  rings,
};

export class SampleTechRadarApi implements TechRadarApi {
  async load() {
    return mock;
  }
}
