# Front-end build container

先確認後端與DB的container都已經包好且docker-bridge-network已建立好

## Follow the step 
```bash
yarn install
```

將frontend backend DB三個container都放在同一個docker-bridge-network


```bash
cd packages/app
```
```bash
yarn backstage-cli package build
```
```bash
cd ../..
```

```bash
docker image build . \
 --tag backstage-frontend:1.0.0
```
複製default.conf到本機電腦，修改$backend name 可使用 
```bash
docker network inspect backstage-bridge
```
來查詢後端name
```bash
docker run -d \
 --network backstage-bridge \
 -p 5000:5000 \
 -v $PWD/default.conf:/etc/nginx/conf.d/default.conf \
 -e TZ=Asia/Taipei \
 --name backstage-frontend backstage-frontend:1.0.0
```
